var Sequelize = require('sequelize');
var sequelize = new Sequelize('postgres://coffee:dlwndud7@localhost:5432/coffee', {
    timezone: '+09:00'
});

sequelize['Member'] = sequelize.define('member', {
    memberId: {
        type: Sequelize.STRING,
        primaryKey: true
    },
    memberName: Sequelize.STRING
});

sequelize['Coffee'] = sequelize.define('coffee', {
    coffeeId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    basicDate: Sequelize.STRING(8),
    memberId: {
        type: Sequelize.STRING,
        references: {
            model: sequelize.Member,
            key: 'memberId',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    comt: Sequelize.STRING,
    desc: Sequelize.STRING
});

module.exports = sequelize;
