var moment = require('moment');
var momentRange = require('moment-range');

momentRange.extendMoment(moment);

module.exports = moment;
