var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var sequelize = require('../sequelize/sequelize');
var moment = require('moment');
var momentRange = require('moment-range');
momentRange.extendMoment(moment);

var coffeeService = require('../service/coffee');

moment.updateLocale('ko', {
    week: { dow: 1 }
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index');
});

router.get('/list', function (req, res, next) {
    sequelize.sync().then(function() {
        return sequelize.Coffee.findAll({
            where: {
                basicDate: {
                    $like: req.query.basicMonth + '__'
                }
            }
        });
    }).then(function(result) {
        res.status(200).json(result);
    });
});

router.post('/save', function (req, res, next) {
    var param = { basicDate: req.body.basicDate, memberId: req.body.memberId };
    coffeeService.save(param).then(function(coffee) {
        res.status(200).json(coffee);
    });
});

router.post('/bulk', function (req, res, next) {
    var param = { basicDate: req.body.basicDate, memberId: req.body.memberId };
    coffeeService.bulk(param).then(function(coffees) {
        res.status(200).json(coffees);
    });
});

router.post('/move', function (req, res, next) {
    var param = { coffeeId: req.body.coffeeId, basicDate: req.body.basicDate, memberId: req.body.memberId };
    coffeeService.move(param).then(function(coffee) {
        res.status(200).json(coffee);
    });
});

router.post('/remove', function (req, res, next) {
    sequelize.sync().bind({

    }).then(function() {
        return sequelize.Coffee.findById(req.body.coffeeId);
    }).then(function(coffee) {
        return coffee.destroy();
    }).then(function() {
        res.status(200).json(true);
    });
});

router.post('/desc', function (req, res, next) {
    sequelize.sync().bind({

    }).then(function() {
        return sequelize.Coffee.findById(req.body.coffeeId);
    }).then(function(coffee) {
        return coffee.update({
            desc: req.body.desc
        });
    }).then(function() {
        res.status(200).json(true);
    });
});

router.post('/init', function (req, res, next) {
    sequelize.sync().bind({

    }).then(function() {
        return sequelize.Coffee.findAll({
            where: {
                basicDate: {
                    $like: req.body.basicMonth + '__'
                }
            }
        })
    }).then(function(coffees) {
        return Promise.all(coffees.map(function(coffee) {
            return coffee.destroy();
        }));
    }).then(function() {
        res.status(200).json(true);
    });
});

module.exports = router;
