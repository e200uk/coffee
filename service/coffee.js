var Sequelize = require('sequelize');
var sequelize = require('../sequelize/sequelize');
var moment = require('../moment/moment');

module.exports = {
    save: function(coffee) {
        return sequelize.sync().bind({
            module: this
        }).then(function() {
            return sequelize.Coffee.findOne({
                where: {
                    basicDate: {
                        $eq: coffee.basicDate
                    }
                }
            })
        }).then(function(exist) {
            this['exist'] = exist;
        }).then(function() {
            if(this.exist && coffee.memberId == 'Coupon') {
                if(this.exist.memberId == 'Coupon' || this.exist.memberId == 'Holiday') return;
                var basicDate = moment(coffee.basicDate, 'YYYYMMDD').add(1, 'd').format('YYYYMMDD');
                var param = { basicDate: basicDate, memberId: this.exist.memberId };
                return this.module.bulk(param);
            }
        }).then(function() {
            if(this.exist) {
                var updated = { memberId: coffee.memberId };
                if(coffee.memberId == 'Holiday' && (this.exist.memberId != 'Holiday' && this.exist.memberId != 'Coupon' && this.exist.memberId != 'Etc')) updated['comt'] = this.exist.memberId;
                if(coffee.memberId == 'Etc' && (this.exist.memberId != 'Holiday' && this.exist.memberId != 'Coupon' && this.exist.memberId != 'Etc')) updated['comt'] = this.exist.memberId;
                return this.exist.update(updated);
            } else {
                var created = { basicDate: coffee.basicDate, memberId: coffee.memberId };
                return sequelize.Coffee.create(created);
            }
        });
    },
    bulk: function(coffee) {
        return sequelize.sync().bind({
            order: ['Amy', 'SS', 'Liz', 'Raphael', 'HB', 'D.VA'],
            queue: ['Amy', 'SS', 'Liz', 'Raphael', 'HB', 'D.VA'],
            ranges: (function() {
                var ranges = moment.range(coffee.basicDate, moment(coffee.basicDate, 'YYYYMM').endOf('month').format('YYYYMMDD'));
                return Array.from((ranges).by('days')).filter(function(range) {
                    return range.weekday() != 5 && range.weekday() != 6;
                });
            })()
        }).then(function() {
            var queue = this.queue;
            this['shiftAndPush'] = function() {
                var item = queue.shift();
                queue.push(item);
                return item;
            }
        }).then(function() {
            for(var i = 0; i < this.order.indexOf(coffee.memberId); i++) {
                this.shiftAndPush();
            }
            return Promise.all(this.ranges.map(function(range, index) {
                return sequelize.Coffee.findOne({
                    where: {
                        basicDate: range.format('YYYYMMDD')
                    }
                });
            }));
        }).then(function(coffees) {
            var me = this;
            return Promise.all(coffees.map(function(coffee, index) {
                if(coffee) {
                    if(coffee.memberId == 'Coupon') {

                    } else if(coffee.memberId == 'Holiday' || coffee.memberId == 'Etc') {
                        var updated = { comt: me.shiftAndPush() };
                        return coffee.update(updated);
                    } else {
                        var updated = { memberId: me.shiftAndPush() };
                        return coffee.update(updated);
                    }
                } else {
                    var created = { basicDate: me.ranges[index].format('YYYYMMDD'), memberId: me.shiftAndPush() };
                    return sequelize.Coffee.create(created);
                }
            }));
        })
    },
    move: function(coffee) {
        return sequelize.sync().bind({

        }).then(function() {
            return sequelize.Coffee.findById(coffee.coffeeId);
        }).then(function(orgin) {
            this['origin'] = orgin;
        }).then(function() {
            return sequelize.Coffee.findOne({
                where: {
                    basicDate: {
                        $eq: coffee.basicDate
                    }
                }
            });
        }).then(function(dest) {
            if(dest) {
                return dest.update({
                    basicDate: this.origin.basicDate
                });
            }
        }).then(function() {
            return this.origin.update({
                basicDate: coffee.basicDate
            });
        });
    }
};
